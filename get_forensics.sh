#!/bin/bash

if [ "$#" -ne 1 ] && [ "$#" -ne 2 ]
then
    echo "   Error! Illegal number parameters. Usage:"
    echo "      get_forensics forensics_ID"
    echo "      get_forensics forensics_ID ENV"
    exit 42
else
    if [ "$#" -eq 1 ]
    then
	env="dev"
    else
	if [ $2 != 'dev' ] && [ $2 != 'prod' ]
        then
          echo "Illegal eviroment name. Enviroments are:"
          echo "   dev or prod"
          exit 42
        else
	  env=$2
        fi
    fi

    fdir="Forensics_"$env"_"$1

    mkdir $fdir
    cd $fdir

    if  [ $env == "dev" ]
    then
    	curl http://dm.internal-dev.simscale.com/dm/$1 | tar -x
    else
	curl http://dm.internal.simscale.com/dm/$1 | tar -x
    fi
fi

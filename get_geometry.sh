#!/bin/bash

if [ "$#" -ne 2 ]
then
    echo "   Error! Illegal number parameters. Usage:"
    echo "      get_geometry relatedGeometryBasedUuid ENV"
    exit 42
else
    if [ $2 != 'dev' ] && [ $2 != 'prod' ]
    then
        echo "Illegal eviroment name. Enviroments are:"
        echo "   dev or prod"
        exit 42
    else
	env=$2
    fi
fi

internal="internal"
if  [ $env == "dev" ]
then
	internal="internal-dev"
fi
endpoint="http://service."$internal".simscale.com/internal-api/v1/geobase/$1"

geometryBaseObjectUuid=$(curl $endpoint | jq '.objectId' | xargs)
format=$(curl $endpoint | jq '.format' | xargs)

echo $geometryBaseObjectUuid
echo $format

endpointgeo="http://dm."$internal".simscale.com/dm/$geometryBaseObjectUuid"

ofile="internal-geo.x_t"
if [ $format == "STL" ]
then
	ofile="internal-geo.stl"
fi     
curl $endpointgeo -o $ofile


#!/bin/bash
env=""
if [ "$#" -ne 2 ]
then
    echo "   Error! Illegal number parameters. Usage:"
    echo "      get_app_version APP_NAME ENV"
    exit 42
else
    if [ $2 == 'dev' ] || [ $2 == 'prod' ]
    then
       env=$2
    elif grep -q "slot" <<< $2
    then
	env="dev-"$2
    else
	echo "Illegal eviroment name $2. Valid enviroments are:"
        echo "   dev, prod or slotX"
        exit 42
    fi
fi

echo "$1 version on $env is"
aws ssm get-parameter --name /$env/app/$1/VERSION | jq -r .Parameter.Value

#!/bin/bash

RED='\033[0;31m'
NC='\033[0m'
BTXT=$(tput bold)
NTXT=$(tput sgr0)

port="8081"
uuid="null"
format="true"

if [ $# -eq 0 ]
then
    echo "No arguments supplied, exiting ..."
    exit 1
fi

while getopts ":p:u:f:" opt; do
  case $opt in
      p)
	        port="$OPTARG"
          ;;
      u)
                uuid="$OPTARG"
          ;;
      f)  	
		format="true"
          ;;
      \?)
	        echo "Invalid option -$OPTARG" >&2
	        echo "Valid options:"
	        echo "-p port for the spec service [8081]"
	        echo "-u uuid of the spec"
	        echo "-f format the spec [true]"

          exit 42
          ;;
  esac
done


if [ $env != 'dev' ] && [ $env != 'prod' ] && [ $env != 'stg' ]
then
    echo -e "${RED}Illegal eviroment name: "$env
    echo -e "Enviroments can be: dev, prod or stg${NC}"
    exit 42
fi

echo -e "${RED}using port: ${port}${NC}"


if [ $uuid == 'null' ]
then
    echo -e "${RED}spec uuid not provided${NC}"
    exit 42
fi

entrypoint="http://localhost:"$port"/spec/simulation/snapshot/"$uuid

echo ${entrypoint}

if [ $format == 'false' ]
then
	curl $entrypoint
else
	curl $entrypoint | jq
fi

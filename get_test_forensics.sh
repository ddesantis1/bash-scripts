#!/bin/bash

if [ $# -eq 0 ]
then
    echo "Date required input, exiting ..."
    exit 1
fi

if [[ ! "$1" =~ \d{4}-\d{2}-\d{2} ]]; then
   echo "Date not in format yyyy-mm-dd, exiting ..."
   exit 1
fi

cases=$(aws s3 ls s3://simscale-test-results/jobmind/ | grep "PRE $1")
Field_Separator=$IFS
IFS=/

for val in ${cases}; do
    dir="$(echo -e "${val}" | tr -d '[:space:]' | cut -c 4-)"
    mkdir $dir
    cd $dir
    
    file=$(aws s3 ls s3://simscale-test-results/jobmind/$dir/ | rev | cut -d " " -f1 | rev)
    echo $dir "-->" $file
    aws s3 cp s3://simscale-test-results/jobmind/$dir/$file .
    cd ..
done


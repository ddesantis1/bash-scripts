#!/bin/bash

if [ "$#" -ne 2 ]
then
    echo "   Error! Illegal number parameters. Usage:"
    echo "      get_table tableUuid ENV"
    exit 42
else
    if [ $2 != 'dev' ] && [ $2 != 'prod' ]
    then
        echo "Illegal eviroment name. Enviroments are:"
        echo "   dev or prod"
        exit 42
    else
	env=$2
    fi
fi

internal="internal"
if  [ $env == "dev" ]
then
	internal="internal-dev"
fi
endpoint="http://dm."$internal".simscale.com/dm/$1"
curl $endpoint -o table.csv

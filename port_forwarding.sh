#!/bin/bash

RED='\033[0;31m'
NC='\033[0m'
BTXT=$(tput bold)
NTXT=$(tput sgr0)

env="null"
action="null"

if [ $# -eq 0 ]
then
    echo "No arguments supplied, exiting ..."
    exit 1
fi

if [ $1 == '-k' ]
then
    echo "Killing all the cukectl processes" 
    killall kubectl
    exit 1
fi

while getopts ":e:k:" opt; do  
  case $opt in
      e)
	  env="$OPTARG"
	  action="run"
      ;;
      \?)
	  echo "Invalid option -$OPTARG" >&2
	  echo "Options:"
	  echo "-e env run forfard port on env"
	  echo "-k kill all the kubectl processes"
          exit 1
    ;;
  esac

  case $OPTARG in
    -*) echo "Option $opt needs a valid argument"
    exit 1
    ;;
  esac
done

if [ $env != 'dev' ] && [ $env != 'prod' ] && [ $env != 'std' ]
then
    echo -e "${RED}Illegal eviroment name: "$env  
    echo -e "Enviroments can be: dev, prod or stg${NC}"
    exit 42
fi

kubectl config use-context "aws-"$env > /dev/null

te=$(kubectl get pods -n topoentities | tail -n 1 | cut -c1-29| xargs)
pte=$(kubectl describe pods -n topoentities | grep -i port | head -n 1 | cut -c19-24| xargs)
echo $te "used as pod for the topoeentities on port" $pte
    
ud=$(kubectl get pods -n userdata | tail -n 1 | cut -c1-27| xargs)
pud=$(kubectl describe pods -n userdata | grep -i port | head -n 1 | cut -c19-24| xargs)
echo $ud "used as pod for the userdata on port" $pud

sp=$(kubectl get pods -n specs | tail -n 1 | cut -c1-22| xargs)
psp=$(kubectl describe pods -n specs | grep -i port | head -n 1 | cut -c19-24| xargs)
echo $sp "used as pod for specs on port" $psp

echo -e "Export the following variables in pycharm:"
echo -e "${RED}${BTXT}export TOPOENTITIES_LOCAL_PORT="$pte
echo -e "export USERDATA_LOCAL_PORT="$pud
echo -e "export SPECS_LOCAL_PORT="$psp${NC}${NTXT}
    
kubectl port-forward -n topoentities $te $pte:$pte > /dev/null &
kubectl port-forward -n userdata $ud $pud:$pud > /dev/null &
kubectl port-forward -n specs $sp $psp:$psp > /dev/null &
